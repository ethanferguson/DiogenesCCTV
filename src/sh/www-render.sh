#!/bin/sh
# $Diogenes: www-render.sh 0.1 2020/05/13 05:20:00 geoghegan $

# Copyright (c) 2020 Jordan Geoghegan <jordan@geoghegan.ca>

# Permission to use, copy, modify, and/or distribute this software for any 
# purpose with or without fee is hereby granted, provided that the above 
# copyright notice and this permission notice appear in all copies.

# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH 
# REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
# AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
# INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM 
# LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE 
# OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR 
# PERFORMANCE OF THIS SOFTWARE.

### Global Configuration
# Operating System type declaration
ostype="$1"
# Path to web directory
web_dir="$2"
# Recording Storage Quota (in MiB)
storage_quota="$3"

# misc variables
# This gets a little greasy as we're trying to support Busybox, GNU and BSD ifconfig formatting with a single command.
# my_ip=$(ifconfig | awk '{gsub("/[0-9]?[0-9]$","",$2)}; {gsub("addr:","",$2)}; /inet / {if ($1="inet" && $2!="127\.0\.0\.1") {print $2}}' 2>/dev/null | head -n1)

modal_gen () {
	echo "<br><br><br><p><b><a href=\"video-recordings/$camera_name/$video_name\">$video_name_strip</a></b> (Click to Download)</p><button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#modal\" onclick=\"filename = '$video_name_strip'; filepath = 'video-recordings/$camera_name/$video_name'; filesize = 'File Size: $mb_count MiB'; getElementById('ModalTitle').innerHTML=filename; getElementById('filesize').innerHTML=filesize; document.getElementById('VideoSource').setAttribute('src', filepath); document.getElementById('DLbutton').setAttribute('href', filepath);\">View Video</button>"	
}

#####################################################################
# Linux Renderer
#####################################################################

linux_render () {
	# Put resource usage stats into variables to be called from sed
	# bc is set to a large accuracy depth because some stats are calculated indirectly, which can lead to issues with \
	# accumlative rounding errors when using lower accuracy depth. We use POSIX shell substring manipulation to strip the decimal place
	cpu_percent_used=$(top -b -n 2 | awk '{IGNORECASE = 1} /^.?CPU/ {gsub("id,","100",$8); print 100-$8}' | tail -n 1)
	phys_mem_bytes=$(awk '/^MemTotal:/ {print $2 * 1024}' /proc/meminfo)
	phys_mem_mb=$(($phys_mem_bytes/1048576))
	ram_percent_used=$(echo "scale=10; 100 - ( $(awk '/^MemFree:/ {print $2 * 1024}' /proc/meminfo) / $phys_mem_bytes * 100)" | bc)
	ram_mb_used=$(echo "scale=10; ( $phys_mem_bytes - $(awk '/^MemFree:/ {print $2 * 1024}' /proc/meminfo) ) / 1048576" | bc)
	disk_use_mb=$(find "$web_dir"/video-recordings -type f -exec stat -c "%s" {} + | awk '{sum+=$1;} END{printf "%4.1f", sum/1048576;}')
	disk_use_gb=$(echo "scale=2; $disk_use_mb / 1024" | bc | sed 's/^\./0./g')
	quota_gb=$(echo "scale=2; $storage_quota / 1024" | bc)
	disk_percent_used=$(echo "scale=10;( $disk_use_mb / $storage_quota) * 100" | bc | sed 's/^\./0./g')
	cur_date=$(date "+%a %b %e %Y %Z")
	num_cameras=$(stat -c "%s" "$web_dir"/video-recordings/* | wc -l)
	template_dir=$(mktemp -d)

	# set temp files for index.html generation
	index_tmp=$(mktemp)
	index_atomic=$(mktemp -p "$web_dir")
	
	# set temp files for admin.html generation
	admin_tmp=$(mktemp)
	admin_atomic=$(mktemp -p "$web_dir")

	# set temp files for timelapse.html generation
	timelapse_tmp=$(mktemp)
	timelapse_atomic=$(mktemp -p "$web_dir")

	# set temp files for utilities.html generation
	utilities_tmp=$(mktemp)
	utilities_atomic=$(mktemp -p "$web_dir")
	
	# Copy templates dir to MFS storage
	find "$web_dir/www_templates/" -type f -print0 | xargs -0 -I{} cp {} "$template_dir"/


### Generate Dashboard (index.html)

	# Use variables defined above to fill in resource usage stat bars and other page info
	# We use POSIX shell substring manipulation to strip decimals  
	sed -e "s/CPU_USAGE_PERCENT/${cpu_percent_used%%.*}/g" \
	    -e "s/RAM_USAGE_PERCENT/${ram_percent_used%%.*}/g" \
	    -e "s/AMOUNT_RAM_USED/${ram_mb_used%%.*}/g" \
	    -e "s/PHYSICAL_RAM_INSTALLED/$phys_mem_mb/g" \
	    -e "s/DISK_USAGE_PERCENT/${disk_percent_used%%.*}/g" \
	    -e "s/AMOUNT_DISK_USED/$disk_use_gb/g" \
	    -e "s/AMOUNT_DISK_QUOTA/$quota_gb/g" \
	    -e "s/THE_CURRENT_DATE/$cur_date/g" \
	    -e "s/THE_NUMBER_OF_CAMERAS/$num_cameras/g" \
	    -e "s/PAGE_TITLE_PLACEHOLDER/Dashboard/g" "$template_dir"/head.html > "$index_tmp"

	# Fill in individual camera links in side bar
	for friendlyname_index in "$web_dir"/video-recordings/* ; do
		sed -i -e '/^<!--Insert_Camera_Below_Me-->/a\' -e "<a class=\"collapse-item\" href=\"${friendlyname_index##*/}.html\">${friendlyname_index##*/}</a>|" "$index_tmp"
	done

	# Add HLS video streams to dashboard
	echo '<br><br><br>' >> "$index_tmp"
	echo '<div class="container">' >> "$index_tmp"
	echo '<div class="row">' >> "$index_tmp"

	for hls_stream in "$web_dir"/hls/* ; do 
		sed -e "s/FRIENDLY_NAME/${hls_stream##*/}/g" -e '/^\/\//d' -e '/^<!--/d' "$template_dir"/video-js.html >> "$index_tmp"
	done

	echo '<br><br><br>' >> "$index_tmp"
	echo '</div></div>' >> "$index_tmp"
	cat "$template_dir"/tail.html >> "$index_tmp"

	# Perform safe atomic move to web root
	# POSIX sed doesnt support adding newlines, so we use '|' as a placeholder and replace it with a proper newline using 'tr'
	tr '|' '\n' < "$index_tmp" > "$index_atomic" && mv -f "$index_atomic" "$web_dir"/index.html && chmod 644 "$web_dir"/index.html
	rm -f "$index_tmp"


### Generate Admin Page

	# Use variables defined above to fill in resource usage stat bars and other page info
	# We use POSIX shell substring manipulation to strip decimals  
	sed -e "s/CPU_USAGE_PERCENT/${cpu_percent_used%%.*}/g" \
	    -e "s/RAM_USAGE_PERCENT/${ram_percent_used%%.*}/g" \
	    -e "s/AMOUNT_RAM_USED/${ram_mb_used%%.*}/g" \
	    -e "s/PHYSICAL_RAM_INSTALLED/$phys_mem_mb/g" \
	    -e "s/DISK_USAGE_PERCENT/${disk_percent_used%%.*}/g" \
	    -e "s/AMOUNT_DISK_USED/$disk_use_gb/g" \
	    -e "s/AMOUNT_DISK_QUOTA/$quota_gb/g" \
	    -e "s/THE_CURRENT_DATE/$cur_date/g" \
	    -e "s/THE_NUMBER_OF_CAMERAS/$num_cameras/g" \
	    -e "s/PAGE_TITLE_PLACEHOLDER/Admin Dashboard/g" "$template_dir"/head.html > "$admin_tmp"

	# Fill in individual camera links in side bar
	for friendlyname_index in "$web_dir"/video-recordings/* ; do
		sed -i -e '/^<!--Insert_Camera_Below_Me-->/a\' -e "<a class=\"collapse-item\" href=\"${friendlyname_index##*/}.html\">${friendlyname_index##*/}</a>|" "$admin_tmp"
	done

	# Do Work Here

	echo '</div></div>' >> "$admin_tmp"
	cat "$template_dir"/tail.html >> "$admin_tmp"

	# Perform safe atomic move to web root
	# POSIX sed doesnt support adding newlines, so we use '|' as a placeholder and replace it with a proper newline using 'tr'
	tr '|' '\n' < "$admin_tmp" > "$admin_atomic" && mv -f "$admin_atomic" "$web_dir"/admin.html && chmod 644 "$web_dir"/admin.html
	rm -f "$admin_tmp"

### Generate Timelapse Page

	# Use variables defined above to fill in resource usage stat bars and other page info
	# We use POSIX shell substring manipulation to strip decimals  
	sed -e "s/CPU_USAGE_PERCENT/${cpu_percent_used%%.*}/g" \
	    -e "s/RAM_USAGE_PERCENT/${ram_percent_used%%.*}/g" \
	    -e "s/AMOUNT_RAM_USED/${ram_mb_used%%.*}/g" \
	    -e "s/PHYSICAL_RAM_INSTALLED/$phys_mem_mb/g" \
	    -e "s/DISK_USAGE_PERCENT/${disk_percent_used%%.*}/g" \
	    -e "s/AMOUNT_DISK_USED/$disk_use_gb/g" \
	    -e "s/AMOUNT_DISK_QUOTA/$quota_gb/g" \
	    -e "s/THE_CURRENT_DATE/$cur_date/g" \
	    -e "s/THE_NUMBER_OF_CAMERAS/$num_cameras/g" \
	    -e "s/PAGE_TITLE_PLACEHOLDER/Timelapse/g" "$template_dir"/head.html > "$timelapse_tmp"

	# Fill in individual camera links in side bar
	for friendlyname_index in "$web_dir"/video-recordings/* ; do
		sed -i -e '/^<!--Insert_Camera_Below_Me-->/a\' -e "<a class=\"collapse-item\" href=\"${friendlyname_index##*/}.html\">${friendlyname_index##*/}</a>|" "$timelapse_tmp"
	done

	# Do Work Here

	echo '</div></div>' >> "$timelapse_tmp"
	cat "$template_dir"/tail.html >> "$timelapse_tmp"

	# Perform safe atomic move to web root
	# POSIX sed doesnt support adding newlines, so we use '|' as a placeholder and replace it with a proper newline using 'tr'
	tr '|' '\n' < "$timelapse_tmp" > "$timelapse_atomic" && mv -f "$timelapse_atomic" "$web_dir"/timelapse.html && chmod 644 "$web_dir"/timelapse.html
	rm -f "$timelapse_tmp"

### Generate Utilities Page

	# Use variables defined above to fill in resource usage stat bars and other page info
	# We use POSIX shell substring manipulation to strip decimals  
	sed -e "s/CPU_USAGE_PERCENT/${cpu_percent_used%%.*}/g" \
	    -e "s/RAM_USAGE_PERCENT/${ram_percent_used%%.*}/g" \
	    -e "s/AMOUNT_RAM_USED/${ram_mb_used%%.*}/g" \
	    -e "s/PHYSICAL_RAM_INSTALLED/$phys_mem_mb/g" \
	    -e "s/DISK_USAGE_PERCENT/${disk_percent_used%%.*}/g" \
	    -e "s/AMOUNT_DISK_USED/$disk_use_gb/g" \
	    -e "s/AMOUNT_DISK_QUOTA/$quota_gb/g" \
	    -e "s/THE_CURRENT_DATE/$cur_date/g" \
	    -e "s/THE_NUMBER_OF_CAMERAS/$num_cameras/g" \
	    -e "s/PAGE_TITLE_PLACEHOLDER/Utilities/g" "$template_dir"/head.html > "$utilities_tmp"

	# Fill in individual camera links in side bar
	for friendlyname_index in "$web_dir"/video-recordings/* ; do
		sed -i -e '/^<!--Insert_Camera_Below_Me-->/a\' -e "<a class=\"collapse-item\" href=\"${friendlyname_index##*/}.html\">${friendlyname_index##*/}</a>|" "$utilities_tmp"
	done

	# Do Work Here

	echo '</div></div>' >> "$utilities_tmp"
	cat "$template_dir"/tail.html >> "$utilities_tmp"

	# Perform safe atomic move to web root
	# POSIX sed doesnt support adding newlines, so we use '|' as a placeholder and replace it with a proper newline using 'tr'
	tr '|' '\n' < "$utilities_tmp" > "$utilities_atomic" && mv -f "$utilities_atomic" "$web_dir"/utilities.html && chmod 644 "$web_dir"/utilities.html
	rm -f "$utilities_tmp"

### Generate Recording pages for each Camera

	# cd into each individual cameras recordings folder and generate a page to view the recordings from
	for friendlyname in "$web_dir"/video-recordings/* ; do
		tmpfile=$(mktemp)
		atomic_tmp=$(mktemp -p "$web_dir")
		video_count=$(find "$web_dir"/video-recordings/"${friendlyname##*/}"/ -type f -name "*.mp4" -exec stat -c "%s" {} + | wc -l)
		oldest_video=$(ls -rt "$friendlyname" | head -n1)
		newest_video=$(ls -t "$friendlyname" | head -n2 | tail -1)
		storage_used=$(find "$friendlyname" -type f -exec stat -c "%s" {} + | awk '{sum+=$1;} END{printf "%4.1f", sum/1048576;}')
		quota_used=$(echo "scale=10; ($storage_used / $storage_quota) * 100" | bc | sed 's/^\./0./g')
		camera_name=${friendlyname##*/}
		

		# Use variables defined above to fill in resource usage stat bars and other page info
		# We use shell substring manipulation to strip the decimal place  
		sed -e "s/CPU_USAGE_PERCENT/${cpu_percent_used%%.*}/g" \
	    	    -e "s/RAM_USAGE_PERCENT/${ram_percent_used%%.*}/g" \
	    	    -e "s/AMOUNT_RAM_USED/${ram_mb_used%%.*}/g" \
	    	    -e "s/PHYSICAL_RAM_INSTALLED/$phys_mem_mb/g" \
	    	    -e "s/DISK_USAGE_PERCENT/${disk_percent_used%%.*}/g" \
	    	    -e "s/AMOUNT_DISK_USED/$disk_use_gb/g" \
	    	    -e "s/AMOUNT_DISK_QUOTA/$quota_gb/g" \
		    -e "s/THE_CURRENT_DATE/$cur_date/g" \
		    -e "s/THE_NUMBER_OF_CAMERAS/$num_cameras/g" \
		    -e "s/PAGE_TITLE_PLACEHOLDER/${friendlyname##*/} Recordings/g" "$template_dir"/head.html > "$tmpfile"

		# Fill in individual camera links in side bar		
		for cameras in "$web_dir"/video-recordings/* ; do
			sed -i -e '/^<!--Insert_Camera_Below_Me-->/a\' -e "<a class=\"collapse-item\" href=\"${cameras##*/}.html\">${cameras##*/}</a>|" "$tmpfile"
		done 

		# Fill in stats
		echo "<p><b>Page Generated on:</b> $(date)</p>"  >> "$tmpfile"
		echo "<p><b>Storage Used:</b> $(echo "scale=2; $storage_used / 1024" | bc | sed 's/^\./0./g') GiB (${quota_used%%.*}% of $quota_gb GiB Quota)</p>" >> "$tmpfile"
		echo "<p><b>Number of Recordings:</b> $video_count</p>" >> "$tmpfile" 
		echo "<p><b>Newest Video:</b> $newest_video</p>" >> "$tmpfile" 
		echo "<p><b>Oldest Video:</b> $oldest_video</p>" >> "$tmpfile" 

		# Iterate over every video file in each camera dir to generate the web page for accessing the footage
		# We use 'tail +2' here because we dont want to display the currently in-progress recording
		for video_file in $(find "$friendlyname" -type f -name "*.mp4" -exec stat -c "%s+%n" {} + | sed '1!G;h;$!d' | tail +2) ; do
			# Set variables to be generated for each video file
			video_name="${video_file##*/}"
			video_name_strip=${video_name%.mp4}
			byte_count="${video_file%+*}" # 
			mb_count=$(($byte_count/1048576))  # Use shell integer arithmetic to avoid forking as this may be called thousands of times 
			
			# Generate sorted list of recordings [see modal_gen function defined at top of file]
			modal_gen 
		done >> "$tmpfile"

		cat "$template_dir"/tail.html >> "$tmpfile"
		# Perform safe atomic move to web root
		# POSIX sed doesnt support adding newlines, so we use '|' as a placeholder and replace it with a proper newline using 'tr'
		tr '|' '\n' < "$tmpfile" > "$atomic_tmp" && mv -f "$atomic_tmp" "$web_dir"/"${friendlyname##*/}".html && chmod 644 "$web_dir"/"${friendlyname##*/}".html
		rm -f "$tmpfile"
	done
	rm -rf "$template_dir"
}

#####################################################################
# OpenBSD Renderer
#####################################################################

openbsd_render () {
	# Put resource usage stats into variables to be called from sed
	# bc is set to a large accuracy depth because some stats are calculated indirectly, which can lead to issues with \
	# accumlative rounding errors when using lower accuracy depth. We use POSIX shell substring manipulation to strip the decimal place
	cpu_percent_used=$(vmstat 1 2 | awk 'END{print 100-$18}')
	phys_mem_bytes=$(sysctl -n hw.physmem)
	phys_mem_mb=$(($phys_mem_bytes/1048576))
	ram_percent_used=$(echo "scale=10; 100 - $(vmstat | awk 'END{ print $4 * 104857600 }') / $phys_mem_bytes" | bc)
	ram_mb_used=$(echo "scale=10; ( $phys_mem_bytes - $(vmstat | awk 'END{print $4 * 1048576}') ) / 1048576" | bc)
	disk_use_mb=$(find "$web_dir"/video-recordings -type f -exec stat -f "%z" {} + | awk '{sum+=$1;} END{printf "%4.1f", sum/1048576;}')
	disk_use_gb=$(echo "scale=2; $disk_use_mb / 1024" | bc | sed 's/^\./0./g')
	quota_gb=$(echo "scale=2; $storage_quota / 1024" | bc)
	disk_percent_used=$(echo "scale=10;( $disk_use_mb / $storage_quota) * 100" | bc | sed 's/^\./0./g')
	cur_date=$(date "+%a %b %e %Y %Z")
	num_cameras=$(stat -f "%z" "$web_dir"/video-recordings/* | wc -l)
	template_dir=$(mktemp -d)

	# set temp files for index.html generation
	index_tmp=$(mktemp)
	index_atomic=$(mktemp -p "$web_dir")
	
	# set temp files for admin.html generation
	admin_tmp=$(mktemp)
	admin_atomic=$(mktemp -p "$web_dir")

	# set temp files for timelapse.html generation
	timelapse_tmp=$(mktemp)
	timelapse_atomic=$(mktemp -p "$web_dir")

	# set temp files for utilities.html generation
	utilities_tmp=$(mktemp)
	utilities_atomic=$(mktemp -p "$web_dir")

	# Copy templates dir to MFS storage
	find "$web_dir/www_templates/" -type f -print0 | xargs -0 -I{} cp {} "$template_dir"/


### Generate Dashboard (index.html)

	# Use variables defined above to fill in resource usage stat bars and other page info
	# We use POSIX shell substring manipulation to strip decimals  
	sed -e "s/CPU_USAGE_PERCENT/${cpu_percent_used%%.*}/g" \
	    -e "s/RAM_USAGE_PERCENT/${ram_percent_used%%.*}/g" \
	    -e "s/AMOUNT_RAM_USED/${ram_mb_used%%.*}/g" \
	    -e "s/PHYSICAL_RAM_INSTALLED/$phys_mem_mb/g" \
	    -e "s/DISK_USAGE_PERCENT/${disk_percent_used%%.*}/g" \
	    -e "s/AMOUNT_DISK_USED/$disk_use_gb/g" \
	    -e "s/AMOUNT_DISK_QUOTA/$quota_gb/g" \
	    -e "s/THE_CURRENT_DATE/$cur_date/g" \
	    -e "s/THE_NUMBER_OF_CAMERAS/$num_cameras/g" \
	    -e "s/PAGE_TITLE_PLACEHOLDER/Dashboard/g" "$template_dir"/head.html > "$index_tmp"

	# Fill in individual camera links in side bar
	for friendlyname_index in "$web_dir"/video-recordings/* ; do
		sed -i -e '/^<!--Insert_Camera_Below_Me-->/a\' -e "<a class=\"collapse-item\" href=\"${friendlyname_index##*/}.html\">${friendlyname_index##*/}</a>|" "$index_tmp"
	done

	# Add HLS video streams to dashboard
	echo '<br><br><br>' >> "$index_tmp"
	echo '<div class="container">' >> "$index_tmp"
	echo '<div class="row">' >> "$index_tmp"

	for hls_stream in "$web_dir"/hls/* ; do 
		sed -e "s/FRIENDLY_NAME/${hls_stream##*/}/g" -e '/^\/\//d' -e '/^<!--/d' "$template_dir"/video-js.html >> "$index_tmp"
	done

	echo '<br><br><br>' >> "$index_tmp"
	echo '</div></div>' >> "$index_tmp"
	cat "$template_dir"/tail.html >> "$index_tmp"

	# Perform safe atomic move to web root
	# POSIX sed doesnt support adding newlines, so we use '|' as a placeholder and replace it with a proper newline using 'tr'
	tr '|' '\n' < "$index_tmp" > "$index_atomic" && mv -f "$index_atomic" "$web_dir"/index.html && chmod 644 "$web_dir"/index.html
	rm -f "$index_tmp"


### Generate Admin Page

	# Use variables defined above to fill in resource usage stat bars and other page info
	# We use POSIX shell substring manipulation to strip decimals  
	sed -e "s/CPU_USAGE_PERCENT/${cpu_percent_used%%.*}/g" \
	    -e "s/RAM_USAGE_PERCENT/${ram_percent_used%%.*}/g" \
	    -e "s/AMOUNT_RAM_USED/${ram_mb_used%%.*}/g" \
	    -e "s/PHYSICAL_RAM_INSTALLED/$phys_mem_mb/g" \
	    -e "s/DISK_USAGE_PERCENT/${disk_percent_used%%.*}/g" \
	    -e "s/AMOUNT_DISK_USED/$disk_use_gb/g" \
	    -e "s/AMOUNT_DISK_QUOTA/$quota_gb/g" \
	    -e "s/THE_CURRENT_DATE/$cur_date/g" \
	    -e "s/THE_NUMBER_OF_CAMERAS/$num_cameras/g" \
	    -e "s/PAGE_TITLE_PLACEHOLDER/Admin Dashboard/g" "$template_dir"/head.html > "$admin_tmp"

	# Fill in individual camera links in side bar
	for friendlyname_index in "$web_dir"/video-recordings/* ; do
		sed -i -e '/^<!--Insert_Camera_Below_Me-->/a\' -e "<a class=\"collapse-item\" href=\"${friendlyname_index##*/}.html\">${friendlyname_index##*/}</a>|" "$admin_tmp"
	done

	# Do Work Here

	echo '</div></div>' >> "$admin_tmp"
	cat "$template_dir"/tail.html >> "$admin_tmp"

	# Perform safe atomic move to web root
	# POSIX sed doesnt support adding newlines, so we use '|' as a placeholder and replace it with a proper newline using 'tr'
	tr '|' '\n' < "$admin_tmp" > "$admin_atomic" && mv -f "$admin_atomic" "$web_dir"/admin.html && chmod 644 "$web_dir"/admin.html
	rm -f "$admin_tmp"

### Generate Timelapse Page

	# Use variables defined above to fill in resource usage stat bars and other page info
	# We use POSIX shell substring manipulation to strip decimals  
	sed -e "s/CPU_USAGE_PERCENT/${cpu_percent_used%%.*}/g" \
	    -e "s/RAM_USAGE_PERCENT/${ram_percent_used%%.*}/g" \
	    -e "s/AMOUNT_RAM_USED/${ram_mb_used%%.*}/g" \
	    -e "s/PHYSICAL_RAM_INSTALLED/$phys_mem_mb/g" \
	    -e "s/DISK_USAGE_PERCENT/${disk_percent_used%%.*}/g" \
	    -e "s/AMOUNT_DISK_USED/$disk_use_gb/g" \
	    -e "s/AMOUNT_DISK_QUOTA/$quota_gb/g" \
	    -e "s/THE_CURRENT_DATE/$cur_date/g" \
	    -e "s/THE_NUMBER_OF_CAMERAS/$num_cameras/g" \
	    -e "s/PAGE_TITLE_PLACEHOLDER/Timelapse/g" "$template_dir"/head.html > "$timelapse_tmp"

	# Fill in individual camera links in side bar
	for friendlyname_index in "$web_dir"/video-recordings/* ; do
		sed -i -e '/^<!--Insert_Camera_Below_Me-->/a\' -e "<a class=\"collapse-item\" href=\"${friendlyname_index##*/}.html\">${friendlyname_index##*/}</a>|" "$timelapse_tmp"
	done

	# Do Work Here

	echo '</div></div>' >> "$timelapse_tmp"
	cat "$template_dir"/tail.html >> "$timelapse_tmp"

	# Perform safe atomic move to web root
	# POSIX sed doesnt support adding newlines, so we use '|' as a placeholder and replace it with a proper newline using 'tr'
	tr '|' '\n' < "$timelapse_tmp" > "$timelapse_atomic" && mv -f "$timelapse_atomic" "$web_dir"/timelapse.html && chmod 644 "$web_dir"/timelapse.html
	rm -f "$timelapse_tmp"

### Generate Utilities Page

	# Use variables defined above to fill in resource usage stat bars and other page info
	# We use POSIX shell substring manipulation to strip decimals  
	sed -e "s/CPU_USAGE_PERCENT/${cpu_percent_used%%.*}/g" \
	    -e "s/RAM_USAGE_PERCENT/${ram_percent_used%%.*}/g" \
	    -e "s/AMOUNT_RAM_USED/${ram_mb_used%%.*}/g" \
	    -e "s/PHYSICAL_RAM_INSTALLED/$phys_mem_mb/g" \
	    -e "s/DISK_USAGE_PERCENT/${disk_percent_used%%.*}/g" \
	    -e "s/AMOUNT_DISK_USED/$disk_use_gb/g" \
	    -e "s/AMOUNT_DISK_QUOTA/$quota_gb/g" \
	    -e "s/THE_CURRENT_DATE/$cur_date/g" \
	    -e "s/THE_NUMBER_OF_CAMERAS/$num_cameras/g" \
	    -e "s/PAGE_TITLE_PLACEHOLDER/Utilities/g" "$template_dir"/head.html > "$utilities_tmp"

	# Fill in individual camera links in side bar
	for friendlyname_index in "$web_dir"/video-recordings/* ; do
		sed -i -e '/^<!--Insert_Camera_Below_Me-->/a\' -e "<a class=\"collapse-item\" href=\"${friendlyname_index##*/}.html\">${friendlyname_index##*/}</a>|" "$utilities_tmp"
	done

	# Do Work Here

	echo '</div></div>' >> "$utilities_tmp"
	cat "$template_dir"/tail.html >> "$utilities_tmp"

	# Perform safe atomic move to web root
	# POSIX sed doesnt support adding newlines, so we use '|' as a placeholder and replace it with a proper newline using 'tr'
	tr '|' '\n' < "$utilities_tmp" > "$utilities_atomic" && mv -f "$utilities_atomic" "$web_dir"/utilities.html && chmod 644 "$web_dir"/utilities.html
	rm -f "$utilities_tmp"

### Generate Recording pages for each Camera

	# cd into each individual cameras recordings folder and generate a page to view the recordings from
	for friendlyname in "$web_dir"/video-recordings/* ; do
		tmpfile=$(mktemp)
		atomic_tmp=$(mktemp -p "$web_dir")
		video_count=$(find "$web_dir"/video-recordings/"${friendlyname##*/}"/ -type f -name "*.mp4" -exec stat -f "%z" {} + | wc -l)
		oldest_video=$(ls -rt "$friendlyname" | head -n1)
		newest_video=$(ls -t "$friendlyname" | head -n2 | tail -1)
		storage_used=$(find "$friendlyname" -type f -exec stat -f "%z" {} + | awk '{sum+=$1;} END{printf "%4.1f", sum/1048576;}')
		quota_used=$(echo "scale=10; ($storage_used / $storage_quota) * 100" | bc | sed 's/^\./0./g')
		camera_name=${friendlyname##*/}
		

		# Use variables defined above to fill in resource usage stat bars and other page info
		# We use shell substring manipulation to strip the decimal place  
		sed -e "s/CPU_USAGE_PERCENT/${cpu_percent_used%%.*}/g" \
	    	    -e "s/RAM_USAGE_PERCENT/${ram_percent_used%%.*}/g" \
	    	    -e "s/AMOUNT_RAM_USED/${ram_mb_used%%.*}/g" \
	    	    -e "s/PHYSICAL_RAM_INSTALLED/$phys_mem_mb/g" \
	    	    -e "s/DISK_USAGE_PERCENT/${disk_percent_used%%.*}/g" \
	    	    -e "s/AMOUNT_DISK_USED/$disk_use_gb/g" \
	    	    -e "s/AMOUNT_DISK_QUOTA/$quota_gb/g" \
		    -e "s/THE_CURRENT_DATE/$cur_date/g" \
		    -e "s/THE_NUMBER_OF_CAMERAS/$num_cameras/g" \
		    -e "s/PAGE_TITLE_PLACEHOLDER/${friendlyname##*/} Recordings/g" "$template_dir"/head.html > "$tmpfile"

		# Fill in individual camera links in side bar		
		for cameras in "$web_dir"/video-recordings/* ; do
			sed -i -e '/^<!--Insert_Camera_Below_Me-->/a\' -e "<a class=\"collapse-item\" href=\"${cameras##*/}.html\">${cameras##*/}</a>|" "$tmpfile"
		done 

		# Fill in stats
		echo "<p><b>Page Generated on:</b> $(date)</p>"  >> "$tmpfile"
		echo "<p><b>Storage Used:</b> $(echo "scale=2; $storage_used / 1024" | bc | sed 's/^\./0./g') GiB (${quota_used%%.*}% of $quota_gb GiB Quota)</p>" >> "$tmpfile"
		echo "<p><b>Number of Recordings:</b> $video_count</p>" >> "$tmpfile" 
		echo "<p><b>Newest Video:</b> $newest_video</p>" >> "$tmpfile" 
		echo "<p><b>Oldest Video:</b> $oldest_video</p>" >> "$tmpfile" 

		# Iterate over every video file in each camera dir to generate the web page for accessing the footage
		# We use 'tail +2' here because we dont want to display the currently in-progress recording
		for video_file in $(find "$friendlyname" -type f -name "*.mp4" -exec stat -f "%z+%N" {} + | tail -r | tail +2) ; do
			# Set variables to be generated for each video file
			video_name="${video_file##*/}"
			video_name_strip=${video_name%.mp4}
			byte_count="${video_file%+*}"
			mb_count=$(($byte_count/1048576))  # Use shell integer arithmetic to avoid forking as this will be called thousands of times
	
			# Generate sorted list of recordings [see modal_gen function defined at top of file]
			modal_gen 
		done >> "$tmpfile"

		cat "$template_dir"/tail.html >> "$tmpfile"
		# Perform safe atomic move to web root
		# POSIX sed doesnt support adding newlines, so we use '|' as a placeholder and replace it with a proper newline using 'tr'
		tr '|' '\n' < "$tmpfile" > "$atomic_tmp" && mv -f "$atomic_tmp" "$web_dir"/"${friendlyname##*/}".html && chmod 644 "$web_dir"/"${friendlyname##*/}".html
		rm -f "$tmpfile"
	done
	rm -rf "$template_dir"
}

#####################################################################
# Script Main
#####################################################################

# Check for correct number of arguments
if [ $# -ne 3 ]; then
    printf "\n\nIncorrect number of parameters\nExiting...\n\n" ; exit 2
fi

# Make sure we're running as "_diogenes" user
if [ "$(whoami)" != "_diogenes" ]; then
	printf "\n\nScript must be run as user \"_diogenes\"\nExiting...\n\n" ; exit 1
fi

# Run appropriate function based on "$ostype" variable

# OpenBSD
if [ "$ostype" = "openbsd" ]; then
	openbsd_render
fi

# Linux
if [ "$ostype" = "linux" ]; then
	linux_render
fi
