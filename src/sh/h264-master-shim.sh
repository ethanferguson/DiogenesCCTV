#!/bin/sh
# $Diogenes: master-shim.sh 0.1 2020/05/17 7:00:00 geoghegan $

### Global Configuration
friendlyname="$1"
URL="$2"
vcodec="$3"
acodec="$4"
resolution="$5"
preset="$6"
hw_accel="$7"
buffer_depth="$8"

### FIFO locations
mainFIFO="/home/_diogenes/util/FIFO/$friendlyname/$friendlyname-main.fifo"
gridFIFO="/home/_diogenes/util/FIFO/$friendlyname/$friendlyname-grid.fifo"
liveFIFO="/home/_diogenes/util/FIFO/$friendlyname/$friendlyname-live.fifo"
miscFIFO="/home/_diogenes/util/FIFO/$friendlyname/$friendlyname-misc.fifo"
motionFIFO="/home/_diogenes/util/FIFO/$friendlyname/$friendlyname-motion.fifo"
timelapseFIFO="/home/_diogenes/util/FIFO/$friendlyname/$friendlyname-timelapse.fifo"

### Socket Locations
mainSOCK="/home/_diogenes/util/IPC/$friendlyname/$friendlyname-main.sock"
gridSOCK="/home/_diogenes/util/IPC/$friendlyname/$friendlyname-grid.sock"
liveSOCK="/home/_diogenes/util/IPC/$friendlyname/$friendlyname-live.sock"
miscSOCK="/home/_diogenes/util/IPC/$friendlyname/$friendlyname-misc.sock"
motionSOCK="/home/_diogenes/util/IPC/$friendlyname/$friendlyname-motion.sock"
timelapseSOCK="/home/_diogenes/util/IPC/$friendlyname/$friendlyname-timelapse.sock"

### Functions

# This gets greasy as FFmpeg needs a forced hard exit (bug maybe?)
# Further investigation required...
cleanup () {
	pkill -f "i $URL -vcodec $vcodec -acodec $acodec -s $resolution -preset $preset"
	pkill -f "i $URL -vcodec $vcodec -acodec $acodec -s $resolution -preset $preset"
	pkill -f "i $URL -vcodec $vcodec -acodec $acodec -s $resolution -preset $preset"
	sleep 5
	pkill -9 "i $URL -vcodec $vcodec -acodec $acodec -s $resolution -preset $preset"
	exit
}

ffmpeg_func () {
	# Start master ffmpeg process (and restart ffmpeg process if it crashes)
	while true ;  do 
		ffmpeg -y -hwaccel "$hw_accel" -hide_banner -loglevel warning -err_detect aggressive -fflags discardcorrupt -i "$URL" -vcodec "$vcodec" -acodec "$acodec" -s "$resolution" -preset "$preset" -f tee -tune -zerolatency -use_fifo 1 -fifo_options fifo_format=mpegts:queue_size="$buffer_depth":drop_pkts_on_overflow=1:attempt_recovery=1:recovery_wait_time=1 -map 0:v -map 0:a "[onfail=ignore]$mainFIFO|[onfail=ignore]$gridFIFO|[onfail=ignore]$liveFIFO|[onfail=ignore]$miscFIFO|[onfail=ignore]$motionFIFO|[onfail=ignore]$timelapseFIFO"
		sleep 5
	done
}

mainIPC () {
	tail +1 -f "$mainFIFO" | nc -klU "$mainSOCK"
}

gridIPC () {
	tail +1 -f "$gridFIFO" | nc -klU "$gridSOCK"
}

liveIPC () {
	tail +1 -f "$liveFIFO" | nc -klU "$liveSOCK"
}

miscIPC () {
	tail +1 -f "$miscFIFO" | nc -klU "$miscSOCK"
}

motionIPC () {
	tail +1 -f "$motionFIFO" | nc -klU "$motionSOCK"
}

timelapseIPC () {
	tail +1 -f "$timelapseFIFO" | nc -klU "$timelapseSOCK"
}

### Housekeeping 

# Make sure we're running as "_diogenes" user
if [ "$(whoami)" != "_diogenes" ]; then
	printf "\n\nScript must be run as user \"_diogenes\"\nExiting...\n\n" ; exit 1
fi

# Check for correct number of arguments
if [ $# -ne 8 ]; then
    printf "\n\nUsage: master-shim.sh [1]FriendlyName, [2]URL, [3]VideoCodec, [4]AudioCodec, [5]Resolution, [6]EncodePreset, [7]HW_Accel, [8]BufferDepth\n\nIncorrect number of parameters\nExiting...\n\n" ; exit 2
fi

# Make sure FIFO/Socket directories exist
mkdir -m 775 -p /home/_diogenes/util/FIFO/"$friendlyname"/ 
mkdir -m 775 -p /home/_diogenes/util/IPC/"$friendlyname"/

# Set trap handler
trap cleanup EXIT
trap cleanup INT

### FIFO Management

# If $mainFIFO not exist, create it
if [ -p "$mainFIFO" ]; then
   printf "%s Exists...\nProceeding...\n\n" "$mainFIFO"
else
  rm -f "$mainFIFO" && mkfifo -m 660 "$mainFIFO"
fi

# If $gridFIFO not exist, create it
if [ -p "$gridFIFO" ]; then
   printf "%s Exists...\nProceeding...\n\n" "$gridFIFO"
else
   rm -f "$gridFIFO" && mkfifo -m 660 "$gridFIFO" 
fi

# If $liveFIFO not exist, create it
if [ -p "$liveFIFO" ]; then
   printf "%s Exists...\nProceeding...\n\n" "$liveFIFO"
else
  rm -f "$liveFIFO" && mkfifo -m 664 "$liveFIFO"
fi

# If $miscFIFO not exist, create it
if [ -p "$miscFIFO" ]; then
    printf "%s Exists...\nProceeding...\n\n" "$miscFIFO"
else
   rm -f "$miscFIFO" && mkfifo -m 664 "$miscFIFO"
fi

# If $motionFIFO not exist, create it
if [ -p "$motionFIFO" ]; then
   printf "%s Exists...\nProceeding...\n\n" "$motionFIFO"
else
   rm -f "$motionFIFO" && mkfifo -m 660 "$motionFIFO"
fi

# If $timelapseFIFO not exist, create it
if [ -p "$timelapseFIFO" ]; then
   printf "%s Exists...\nProceeding...\n\n" "$timelapseFIFO"
else
   rm -f "$timelapseFIFO" && mkfifo -m 660 "$timelapseFIFO" 
fi


### Socket Management

# If $mainSOCK exists, delete it
if [ -e "$mainSOCK" ]; then
   rm -f "$mainSOCK"
fi

# If $gridSOCK exists, delete it
if [ -e "$gridSOCK" ]; then
   rm -f "$gridSOCK"
fi

# If $liveSOCK exists, delete it
if [ -e "$liveSOCK" ]; then
   rm -f "$liveSOCK"
fi

# If $miscSOCK exists, delete it
if [ -e "$miscSOCK" ]; then
   rm -f "$miscSOCK"
fi

# If $motionSOCK exists, delete it
if [ -e "$motionSOCK" ]; then
   rm -f "$motionSOCK"
fi

# If $timelapseSOCK exists, delete it
if [ -e "$timelapseSOCK" ]; then
   rm -f "$timelapseSOCK"
fi


# Shim Creation (functions defined above)
mainIPC &
gridIPC &
liveIPC &
miscIPC &
motionIPC &
timelapseIPC &


# Set Socket Permissions (Need to investigate...)
#chmod 660 "$mainsSOCK" "$gridSOCK" "$motionSOCK" "$timelapseSOCK"
#chmod 664 "$liveSOCK" "$miscSOCK"

# Fork ffmpeg process to background (needed so shell can properly trap signals)
ffmpeg_func &

# Occupy main thread (keep shell running so we can perform process/trap management)
while true; do sleep 5 ; done




